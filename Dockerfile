# Specify number (9, 10, 11, ...), not alias (`buster`, `bullseye`).
# Versions below 11 will cause installation of llvm with version lower than desired.
# In the other hand, ghc does not support deb11 yet.
# Using 10 as Debian version seems a fair, working solution compromise for now.
ARG DEBIAN_VERSION='10'

# Glasgow Haskell Compiler and Haskell Tool Stack, with multiplatform support.
FROM debian:${DEBIAN_VERSION}-slim AS final

# A valid version existing at http://downloads.haskell.org/~ghc.
ARG GHC_VERSION='8.10.7'
# Specify number (9, 10, 11, ...), not alias (`buster`, `bullseye`).
ARG DEBIAN_VERSION

# OCI label args.
ARG OCI_AUTHORS='vdSHOP Team'
ARG OCI_DESCRIPTION='Glasgow Haskell Compiler: Multiplatform support.'
ARG OCI_LICENSES='MIT'
ARG OCI_SOURCE='https://gitlab.com/vdshop/public/docker-images/haskell-ghc'
ARG OCI_TITLE='Glasgow Haskell Compiler'
ARG OCI_VENDOR='vdSHOP RedCom S.L.'
ARG OCI_VERSION='${GHC_VERSION}-deb${DEBIAN_VERSION}'

# hadolint ignore=DL3008
RUN apt-get update -y -qq --fix-missing \
  && apt-get install -y --no-install-recommends -qq \
    build-essential \
    libffi-dev \
    libgmp-dev \
    libnuma-dev \
    zlib1g-dev \
    llvm-dev \
    curl \
    ca-certificates \
    git \
    netbase \
    tar \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Copy executables.
COPY usr/local/bin /usr/local/bin/
RUN ["chmod", "-R", "a+x", "/usr/local/bin/"]

# Install GHC and stack.
RUN install-ghc "${GHC_VERSION}" "deb${DEBIAN_VERSION}" '/ghc' && install-stack '/stack'

# Make `ghc` command available everywhere.
ENV PATH="/ghc/bin:${PATH}"

LABEL \
  org.opencontainers.image.authors="${OCI_AUTHORS}" \
  org.opencontainers.image.description="${OCI_DESCRIPTION}" \
  org.opencontainers.image.licenses="${OCI_LICENSES}" \
  org.opencontainers.image.source="${OCI_SOURCE}" \
  org.opencontainers.image.title="${OCI_TITLE}" \
  org.opencontainers.image.vendor="${OCI_VENDOR}" \
  org.opencontainers.image.version="${OCI_VERSION}"
