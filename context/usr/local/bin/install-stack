#!/bin/bash
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit -o nounset -o pipefail -o posix -o noclobber
set -x

# Install Haskell Tool Stack for current platform (os/arch).
# It expects one argument:
# - installation directory (mandatory, e.g. '/stack')
#
# Version is not configurable, since v2.1.3 is the latest with multiplatform support.

install_stack() {
  local stack_dir download_url tar_filename

  stack_dir="${1}"

  [ -n "${stack_dir}" ] || {
    >&2 echo '[!] ERROR: stack_dir argument is mandatory.'
    return 1
  }

  >&2 echo "[+] Preparing directories..."
  rm -rf "${stack_dir}" && mkdir -p "${stack_dir}"

  download_url="$(get-stack-download-url 'commercialhaskell/stack' 'v2.1.3')"
  
  [ -n "${download_url}" ] || {
    >&2 echo '[!] ERROR: Unable to find download urls for commercialhaskell/stack, v2.1.3.'
    return 1
  }
    
  tar_filename="$(basename "${download_url}")"

  cd "${stack_dir}" \
    && >&2 echo "[+] Downloading Haskell Tool Stack tar.gz file, version stack..." \
    && curl -fsSL "${download_url}" -o "${tar_filename}" \
    && >&2 echo '[+] Downloaded tar file.' \
    && >&2 echo '[+] Uncompressing tar file...' \
    && tar -xf "${tar_filename}" \
    && >&2 echo '[+] Uncompressed, cleaning tar file...' \
    && rm -rf "${tar_filename}" \
    && >&2 echo '[+] Making executable' \
    && mv "$(ls -Ad * | grep -Ev '^\.$')/stack" '/usr/local/bin/stack' \
    && chmod 'a+x' '/usr/local/bin/stack' \
    && >&2 echo '[+] Cleaning directory...' \
    && cd / && rm -rf "${stack_dir}"

  >&2 echo '[+] Testing stack executable...' \
    && stack --help

  >&2 echo '[+] Done.'
}

install_stack "${@}"
